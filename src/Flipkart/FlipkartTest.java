package Flipkart;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;


public class FlipkartTest {
    JavascriptExecutor jse;
    WebDriver driver;
    public void searchProduct() throws InterruptedException {
        driver = new ChromeDriver();
        driver.get("https://www.flipkart.com/");
        driver.manage().window().maximize();
        Thread.sleep(500);
        WebElement closeLogin = driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']"));
        closeLogin.click();
        WebElement searchPro = driver.findElement(By.xpath("//input[@class='_3704LK']"));
        searchPro.sendKeys("Shoes");
        searchPro.submit();
//        System.out.println("search");
    }
    public void scrollUp() throws InterruptedException {
        jse = (JavascriptExecutor) driver;
        Thread.sleep(1000);
        jse.executeScript("window.scrollTo(0,300)");
//        System.out.println("scroll");
    }
    public void checkbox() throws InterruptedException {
        Thread.sleep(1000);
       driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[3]/div[1]/div[1]/div")).click();
//        System.out.println("check");
    }
    public void selectItem() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.linkText("Sneakers For Men")).click();
        ArrayList<String> AllTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(AllTabs.get(1));
        jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollTo(0,500)");
//        System.out.println("select");
    }

    public void selectSize() throws InterruptedException {
        Thread.sleep(1000);
        WebElement size = driver.findElement(By.linkText("8"));
        size.click();
        jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollTo(0,20000)");
   }
   public void addCart() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/ul[1]/li[1]/button[1]")).click();
//       driver.findElement(By.xpath("//span[contains(text(),'Cart')]")).click();
//       driver.get("https://flipkart.com/viewcart?otracker=PP_GoToCart");
    }

    public void placeOrder() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.xpath("//span[contains(text(),'Place Order')]")).click();
    }
//    public void login(){
//        driver.findElement(By.linkText("Login")).click();
//        driver.navigate().to("https://www.flipkart.com/viewcart");
//    }

}
