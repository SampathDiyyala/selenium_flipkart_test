package Flipkart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LoginAndSignUp {
    WebDriver driver;

    public void signUp() throws InterruptedException {
        driver = new ChromeDriver();
        Thread.sleep(1000);
        driver.get("https://www.flipkart.com/");
        Thread.sleep(1000);
        WebElement closeLogin = driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']"));
        closeLogin.click();
//        driver.findElement(By.linkText("New to Flipkart? Create an account")).click();
        Thread.sleep(1000);
        WebElement loginEle = driver.findElement(By.linkText("Login"));
        Thread.sleep(1000);
        Actions action = new Actions(driver);
        //mouse curser placed on login button
        action.moveToElement(loginEle).perform();
        Thread.sleep(1000);
        //select myprofile
        driver.findElement(By.cssSelector("._3vhnxf")).click();
//        loginEle.click();
        Thread.sleep(1000);
        //click create an account
        driver.findElement(By.linkText("New to Flipkart? Create an account")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//input[@class='_2IX_2- VJZDxU']")).sendKeys("9806987690");
        //click on Continue
        driver.findElement(By.xpath("//button[@class='_2KpZ6l _2HKlqd _3AWRsL']")).click();
    }

}
